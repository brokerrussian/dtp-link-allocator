const $API = require('./api');
const $config = require('config');

const DTPUrl = $config.get('services.dtp.url');
const oldTimeline = 1524851713;

class Allocator {
  static async all (req, res, next) {
    const path = req.path.replace(/^\//, '');

    if (!path) {
      return res.redirect(DTPUrl);
    }

    const post = await $API.query('getPost/' + path);

    if ((!post.ok && post.error.code === 3) || (post.result.date < oldTimeline)) {
      return res.redirect(`${DTPUrl}${req.path}`);
    }

    if (!post.ok) {
      throw post;
    }

    const telegraphPath = post.result.preview.path;

    res.redirect(`http://telegra.ph/${telegraphPath}`);
  }
}

module.exports = Allocator;