const PORT = require('../../dtp-ecosystem/reference/port');
const ENV = process.env.NODE_ENV;

module.exports = {
  server: {
    port: PORT['dtp-link-allocator'][ENV]
  },
  services: {
    api: {
      url: 'http://127.0.0.1:' + PORT['dtp-api'][ENV]
    },
    dtp: {
      url: ''
    }
  }
};
