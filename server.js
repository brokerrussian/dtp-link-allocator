const $express = require('express');
const $config = require('config');
const app = $express();

const $Allocator = require('./src/modules/allocator');

require('./src/middlewares/body-parser')(app);

app.get('/*', (req, res, next) => {
  $Allocator.all(req, res, next)
    .catch((err) => {
      console.log(err);
      res.end('Server Error!');
    });
});

const PORT = $config.get('server.port');

app.listen(PORT, () => {
  console.log('DTP Link Allocator listing :%s port', PORT);
});